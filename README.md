# Proyecto para aprender a como hacer una página web con HTML5, CSS3, BOOSTRAP 4 y JAVASCRIPT en el front-end, AJAX, PHP y MySQL en el back-end

## Índice:
- [Dependencias](#dependencias)
- [Deploy](#deploy)
- [Versión Web](#version-web)

## Dependencias:
- Live-server, me sirve para tener un servidor local en el navegador y que vaya actualizando los cambios.
- Normalize.css, hace que los navegadores renderizen todos los elementos de forma más consistente y en línea con los standares modernos.

## Deploy:
- He usado Netlify para desplegar mi proyecto,por su fácil us, su rapidez y gratuidad

# Iré actualizando el README al ritmo del proyecto
# Versión Web
- [Netlify](https://venta-de-casas.netlify.app/)